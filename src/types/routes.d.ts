import { RouteConfig } from 'react-router-config';
import { PreloadAction } from 'store/preload';

export interface ExtraRoureProps {
    preload?: PreloadAction | PreloadAction[];
  }
  
export interface AppRoute extends RouteConfig, ExtraRoureProps {
    routes?: AppRoute[];
}
