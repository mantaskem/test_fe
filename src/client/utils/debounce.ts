import { Dispatch } from 'redux';
import { AppState } from 'store/reducer';

const debounces = {};

export const debounceAction = (key: string, callback: any, ms: number) => (
  dispatch: Dispatch<AppState>
) => {
  debounces[key] = setTimeout(() => {
    delete debounces[key];
    dispatch(callback());
  }, ms);
};

export const cancelDebounceAction = (key: string) => () => {
  clearTimeout(debounces[key]);
  delete debounces[key];
};
