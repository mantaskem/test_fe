import { combineReducers } from 'redux';
import UserTimerReducer, { TimerState } from 'features/user/timer/reducer';

export interface UserState {
  timer: TimerState;
}

export default combineReducers({ timer: UserTimerReducer });
