import { AppState } from 'store/reducer';
import { Breakpoint } from 'features/device/constants';

export const isMobileWidth = ({ device }: AppState) => device.breakpoint === Breakpoint.ExtraSmall;

export const isTabletWidth = ({ device }: AppState) =>
  device.breakpoint === Breakpoint.Small || device.breakpoint === Breakpoint.Medium;

export const isLaptopWidth = ({ device }: AppState) => device.breakpoint === Breakpoint.Large;

export const isDesktopWidth = ({ device }: AppState) => device.breakpoint === Breakpoint.ExtraLarge;
