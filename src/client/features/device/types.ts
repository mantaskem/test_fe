import { Breakpoint, ServiceWorkerStatus } from './constants';

export interface DeviceState {
  online: boolean;
  breakpoint: Breakpoint;
  serviceWorkerStatus: ServiceWorkerStatus;
}
