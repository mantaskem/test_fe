import { AppState } from 'store/reducer';
import { increase } from './actions';
import { Dispatch } from 'redux';

export const increaseCount = () => (dispatch: Dispatch<AppState>, getState: () => AppState) => {
  dispatch(increase(1));
};
