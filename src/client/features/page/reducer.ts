import {
  SET_CLIENT_SIDE_ACTIVE,
  SET_SERVER_SIDE_ACTIVE,
  SET_SERVER_SIDE_NOT_ACTIVE,
  SET_CLIENT_SIDE_NOT_ACTIVE,
  SET_LANDING_PAGE_SEEN,
  SET_ABOUT_PAGE_SEEN,
  SET_REGISTRATION_PAGE_SEEN
} from 'features/page/constants';
import { combineReducers } from 'redux';
import { createReducer } from 'utils/redux';

export interface PageState {
  isClientSideActive: boolean;
  isServerSideActive: boolean;
  landingPageSeen: boolean;
  aboutPageSeen: boolean;
  registrationPageSeen: boolean;
}

const defaultState: PageState = {
  isClientSideActive: false,
  isServerSideActive: true,
  landingPageSeen: false,
  aboutPageSeen: false,
  registrationPageSeen: false
};

const clientSideReducer = createReducer(
  {
    [SET_CLIENT_SIDE_ACTIVE]: () => true,
    [SET_CLIENT_SIDE_NOT_ACTIVE]: () => false
  },
  defaultState.isClientSideActive
);

const serverSideReducer = createReducer(
  {
    [SET_SERVER_SIDE_ACTIVE]: () => true,
    [SET_SERVER_SIDE_NOT_ACTIVE]: () => false
  },
  defaultState.isServerSideActive
);

const lpPageVisibilityReducer = createReducer(
  {
    [SET_LANDING_PAGE_SEEN]: () => true
  },
  defaultState.landingPageSeen
);

const aboutPageVisibilityReducer = createReducer(
  {
    [SET_ABOUT_PAGE_SEEN]: () => true
  },
  defaultState.aboutPageSeen
);

const registrationPageVisibilityReducer = createReducer(
  {
    [SET_REGISTRATION_PAGE_SEEN]: () => true
  },
  defaultState.registrationPageSeen
);

export default combineReducers({
  isClientSideActive: clientSideReducer,
  isServerSideActive: serverSideReducer,
  landingPageSeen: lpPageVisibilityReducer,
  aboutPageSeen: aboutPageVisibilityReducer,
  registrationPageSeen: registrationPageVisibilityReducer
});
