import * as React from 'react';

const OfferCard = () => (
  <div className="p10 pb20">
    <div
      className="w100p h350 offerCard bs-dark-grey bg-white curp"
      style={{ objectFit: 'fill', borderRadius: 5 }}>
      <div>
        <img
          src={'http://santansun.com/wp-content/uploads/2018/11/5b7fdeab1900001d035028dc.jpeg'}
          className="w100p h180"
          style={{ borderTopLeftRadius: 5, borderTopRightRadius: 5 }}
        />
        <div>Content</div>
      </div>
    </div>
  </div>
);

export default OfferCard;
