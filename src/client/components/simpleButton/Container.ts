import { connect } from 'react-redux';
import { AppState } from 'store/reducer';
import SimpleButton from 'components/simpleButton/Component';
import { increaseCount } from 'features/simpleCounter/thunks';

const mapStateToPros = (state: AppState) => {
  return {
    text: 'Click me!'
  };
};

const mapDispatchToProps = {
  handleClick: increaseCount
};

export default connect(
  mapStateToPros,
  mapDispatchToProps
)(SimpleButton);
