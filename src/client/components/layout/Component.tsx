import * as React from 'react';

const Layout: React.FC = ({ children }) => <div className="w100p h100p pt60">{children}</div>;

export default Layout;
