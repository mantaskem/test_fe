import { connect } from 'react-redux';
import List from 'components/list/Component';

const mapStateToProps = () => ({
  ids: ['1', '2', '3', '4']
});

export default connect(mapStateToProps)(List);
