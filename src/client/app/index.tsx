import * as React from 'react';
import { Route, Switch } from 'react-router';
import { omit, map } from 'ramda';
import { AppRoute } from 'types/routes';
import { hot } from 'react-hot-loader';
import { initializeClientSide } from 'features/initializer/thunks';
import { AppState } from 'store/reducer';
import { Store } from 'redux';
import ListPage from 'app/list/Container';
import LandingPage from 'app/lp/Container';
import { PATHS } from 'features/page/constants';

interface Props {
  store?: Store<AppState>;
}

export const appRoutes: AppRoute[] = [
  {
    path: PATHS.LANDING_PAGE,
    exact: true,
    component: LandingPage
  },
  {
    path: PATHS.LIST_PAGE,
    exact: true,
    component: ListPage
  }
];

const routesWithoutChildrens = map<AppRoute, Omit<AppRoute, 'routes'>>(omit(['routes']), appRoutes);

export class Pages extends React.Component<Props> {
  public componentDidMount() {
    if (!!this.props.store) {
      this.props.store.dispatch(initializeClientSide());
    }
  }
  public render() {
    return (
      <Switch>
        {routesWithoutChildrens.map(props => (
          <Route key={props.path} {...props} />
        ))}
      </Switch>
    );
  }
}

export const App = hot(module)(Pages);
