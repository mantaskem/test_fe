import { connectRouter, routerMiddleware } from 'connected-react-router';
import { applyMiddleware, createStore, Store } from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension';
import { History } from 'history';
import thunk from 'redux-thunk';
import { AppState, reducer } from 'store/reducer';
import { config } from 'utils/config';

export const configureStore = (history: History, initialState?: AppState): Store<AppState> => {
  const withRouter = connectRouter(history);
  let middleware = applyMiddleware(thunk, routerMiddleware(history));

  if (config.isDev) {
    middleware = composeWithDevTools(middleware);
  }

  const store: Store<AppState> = createStore(withRouter(reducer), initialState!, middleware);

  if (module.hot) {
    module.hot.accept('store/reducer', () => {
      const { reducer: nextReducer } = require('store/reducer');
      store.replaceReducer(withRouter(nextReducer));
    });
  }

  return store;
};
