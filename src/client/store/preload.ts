import { ThunkAction } from 'redux-thunk';
import { AppState } from 'store/reducer';

// tslint:disable: no-any
export type PreloadAction = (...args: any[]) => ThunkAction<Promise<any>, AppState, {}>;

export const globalPreloadActions: PreloadAction[] = [];
