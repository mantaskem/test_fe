import * as LRUCache from 'lru-cache';
import * as express from 'express';

const cacheTime: number = 3600000;

export const cacheMiddleware = express.Router();

export const ssrCache = new LRUCache({
  max: 500,
  maxAge: cacheTime
});

export const getCacheKey = (req: express.Request) => {
  return `${req.url}`;
};
